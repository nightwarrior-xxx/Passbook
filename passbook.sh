#! /bin/bash
#Passbook is a password manager that stores the password with encryption.
#A user can read,write,delete and generate a password corrensponding to username.
#Following are the flags used in Passbook:-
#            --read           ; for reading the password
#            --write          ; for writing the password
#            --generate       ; for generating the password
#            --delete         ; for deleting the password
#            --help           ; for help 

Generate(){
echo "Which type of algorithm you want!!!"
echo "       
        Enter -crypt for choosing crypt algorithm.
        Enter -1 which uses the MD5 based BSD password algorithm 1. 
        Enter -apr1 uses the apr1 algorithm (Apache variant of the BSD algorithm)"
read -p "->" option
read -sp "Enter a phrase which you want in your password:-" phrase
echo "Your generated hashed password is:-"
openssl passwd $option -salt $phrase password
}

Write(){
    zenity --info --title="Precautions" --text="Be carefull with what you are entering"
    read -p "Username:-" username
    read -p "Which type of Account?:-" account
    read -sp "Enter Password:-" password
    echo 
    read -sp "Enter password again:-" password1
    echo
    if [[ "$password" == "$password1" ]]
        then
            chmod 700 .safe
            echo "$username $account $password" >> .safe
            zenity --warning --text="Always use same password asked by openssl. Use password that you have used previous time. Otherwise you will loose your saved passwords"
        #     openssl enc -a --aes-256-cbc -in .safe -out .safe_safe
            Encrypt
            chmod 000 .safe
            zenity --info --title="Done" --text="Your data is saved successfully"
    else 
        zenity --error --title="Mismatch" --text="Both the password does not match with each other \n Please run the command again"
        exit 0        
  fi
}

Read(){
    read -p "Enter the username:-" username
    read -p "Enter the account type:-" account
    #decrypt function 
    Decrypt
   # chmod 600 main2.txt
    echo "The only result I have is "
    chmod 700 .decrypt
    grep $username .decrypt | grep $account .decrypt
    rm -rf .decrypt  
}

Delete(){
echo "You need to be root user" 
echo "For deleting type:- sed --in-place '/username accountType/d'  FILENAME"
}   

Help(){
echo "Passbook is a password manager that stores the password with encryption.
A user can read,write,delete and generate a password corrensponding to username.
Following are the flags used in Passbook:-
          --read           ; for reading the password
          --write          ; for writing the password
          --generate       ; for generating the password 
          --delete         ; for deleting the password
          --help           ; for help 
for reading the password run:-
        /passbook.sh --read 
for writing the password run:-
        /passbook.sh --write
for deleting the password run:-
        /passbook.sh --delete
for generating the password run:-
        /passbook.sh --generate
for help run:-
        /passbook.sh --help" 
} 

Decrypt(){
     openssl aes-256-cbc -d -a  -in .safe_safe -out .decrypt
     chmod 200 .decrypt
}  

Encrypt(){               
     openssl aes-256-cbc -a -salt -in .safe -out .safe_safe
}

toilet -t --filter metal Passbook 
if [[ $1 == "--generate" ]]
    then
             Generate

 elif [[ $1 == "--write" ]]
    then 
             Write

 elif [[ $1 == "--read" ]]
    then 
            Read

 elif [[ $1 == "--delete" ]]
    then    
            Delete
 elif [[ $1 == "--help" ]]
      then 
            Help 

 else 
       Help                                             
fi
