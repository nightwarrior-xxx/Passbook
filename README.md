[![Bash Shell](https://badges.frapsoft.com/bash/v1/bash.png?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![made-with-bash](https://img.shields.io/badge/Made%20with-Bash-1f425f.svg)](https://www.gnu.org/software/bash/)
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)
[![Open Source Love png2](https://badges.frapsoft.com/os/v2/open-source.png?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![Awesome Badges](https://img.shields.io/badge/badges-awesome-green.svg)](https://github.com/Naereen/badges)

# Passbook 

Passbook is a password manager made in bash scripting language. You will rarely find any password manager made in bash. People usally face problems in remembering their passwords. Thus, *passbook* helps you to save your password in system such that no other can access it. It uses openssl encryption.

![Logo](./logo.png)

## Installation Process
    
```
git clone https://gitlab.com/nightwarrior-xxx/Passbook.git
```

**Prerequisite**

- Install *toilet*
```
sudo apt-get install toilet 
```

- Install *Zenity*
```
sudo apt-get install zenity
```
- Install *openssl*
```
sudo apt-get install openssl
```   
- Install *sed*
```
sudo apt-get install sed
```                

## Future Aspects

- Coverting it into a GUI
- Improve UI/UX 

[![ForTheBadge built-by-developers](http://ForTheBadge.com/images/badges/built-by-developers.svg)](https://GitHub.com/Naereen/)
[![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://GitHub.com/Naereen/)
