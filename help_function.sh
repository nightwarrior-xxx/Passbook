#! /bin/bash
help(){
echo "Passbook is a password manager that stores the password with encryption.
A user can read,write,delete and generate a password corrensponding to username.
Following are the flags used in Passbook:-
          --read           ; for reading the password
          --write          ; for writing the password
          --generate       ; for generating the password 
          --delete         ; for deleting the password
          --help           ; for help 
for reading the password run:-
        /patch1.sh --read 
for writing the password run:-
        /patch1.sh --write
for deleting the password run:-
        /patch1.sh --delete
for generating the password run:-
        /patch1.sh --generate
for help run:-
        /patch1.sh --help" 
}    
help
